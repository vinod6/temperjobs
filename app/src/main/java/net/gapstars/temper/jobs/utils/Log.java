package net.gapstars.temper.jobs.utils;

import net.gapstars.temper.jobs.settings.GeneralSettings;

/**
 * Created by Lyca TV on 10/22/2018.
 */

public class Log {

    public static int INFO_LOG = 1;
    public static int ERROR_LOG = 2;
    public static int WARNING_LOG = 3;

    public static void show(String TAG,String MESSAGE,int LOG_TYPE){
        if(GeneralSettings.IS_APP_DEBUG) {
            if(LOG_TYPE==Log.INFO_LOG) {
                android.util.Log.i(TAG, MESSAGE);
            }else if(LOG_TYPE==Log.ERROR_LOG) {
                android.util.Log.e(TAG, MESSAGE);
            }else if(LOG_TYPE==Log.WARNING_LOG) {
                android.util.Log.w(TAG, MESSAGE);
            }
        }
    }

}
