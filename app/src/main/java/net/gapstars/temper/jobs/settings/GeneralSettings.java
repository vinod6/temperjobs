package net.gapstars.temper.jobs.settings;

/**
 * Created by Vinod Madushanka
 */

public class GeneralSettings {

    // APP Debug State
    public static boolean IS_APP_DEBUG = true;

    // Remote Server API
    public static boolean IS_REMOTE_SERVER = true;
}
