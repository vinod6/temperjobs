package net.gapstars.temper.jobs.cache;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Vinod Madushanka
 */


public class CustomCache {

    Context context;
    String prefPoint = "LOCAL_SETTINGS";
    SharedPreferences.Editor editor;
    SharedPreferences resumesetting;

    public CustomCache(Context context){
        this.context = context;
        resumesetting = context.getSharedPreferences(prefPoint, 0);
    }
    public void setLocalCache(String index,String value){
        editor =  context.getSharedPreferences(prefPoint, 0).edit();
        editor.putString(index,value);
        editor.commit();
    }

    public String getLocalCache(String index){
        return resumesetting.getString(index, "none");
    }


}
