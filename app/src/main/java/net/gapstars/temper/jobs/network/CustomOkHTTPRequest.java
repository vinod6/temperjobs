package net.gapstars.temper.jobs.network;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import net.gapstars.temper.jobs.utils.Log;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;

/**
 * Created by Vinod Madushanka
 */

public class CustomOkHTTPRequest {

    private Handler handler;
    private String url;
    private HashMap<String, String> params;
    private HashMap<String, String> headers;
    public Context context;
    private RequestListener listener;
    private long requestTag;

    private String TAG  = "CustomOkHTTPRequest";

    private Call call;

    private boolean cancelled;

    public interface RequestListener {
        void onResponse(String response, long requestTag);

        void onError(String message, long requestTag);
    }

    public CustomOkHTTPRequest(Context context, String url, HashMap<String, String> params, RequestListener listener, long requestTag) {

        Log.show(TAG,"Link >>>> "+url,Log.INFO_LOG);

        handler = new Handler(context.getMainLooper());

        this.url = url;
        this.params = params;
        this.listener = listener;
        this.context = context;
        this.requestTag = requestTag;
    }




    public void sendRequest(Context context) {
        // Set Custom Okhttp client
        OkHttpClient client = PlixOkHttpClient.chachedClient(context);
        try {
            FormBody formBody = null;

            if (params != null) {
                FormBody.Builder formBodyBuilder = new FormBody.Builder();
                for (String key : params.keySet()) {
                    formBodyBuilder.add(key, params.get(key));
                }
                formBody = formBodyBuilder.build();
            }

            okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder()
                    .url(url);

            if (formBody != null) {
                requestBuilder.post(formBody);
            }

            if (headers != null) {
                for (String headerKey : headers.keySet()) {
                    requestBuilder.addHeader(headerKey, headers.get(headerKey));
                }
            }

            call = client.newCall(requestBuilder.build());
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull final IOException e) {
                    call.cancel();
                    if (!cancelled) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                listener.onError(e.getMessage(), requestTag);
                            }
                        });
                    }
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull final okhttp3.Response response) throws IOException {
                    if (!cancelled) {
                        try {
                            ResponseBody body = response.body();
                            if (body != null) {
                                final String responseStr = body.string();

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onResponse(responseStr, requestTag);
                                    }
                                });
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onError(response.message(), requestTag);
                                    }
                                });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelRequest() {
        if (call != null) {
            call.cancel();
        }
        cancelled = true;
    }

}
