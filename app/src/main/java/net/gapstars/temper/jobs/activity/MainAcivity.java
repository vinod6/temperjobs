package net.gapstars.temper.jobs.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.SuperToast;
import com.github.johnpersano.supertoasts.library.utils.ListenerUtils;

import net.gapstars.temper.jobs.R;
import net.gapstars.temper.jobs.settings.GeneralSettings;
import net.gapstars.temper.jobs.base.PlixBaseActivity;
import net.gapstars.temper.jobs.cache.CustomCache;
import net.gapstars.temper.jobs.task.RawManager;
import net.gapstars.temper.jobs.network.CustomOkHTTPRequest;
import net.gapstars.temper.jobs.network.PlixOkHttpClient;
import net.gapstars.temper.jobs.network.RequestGenerator;
import net.gapstars.temper.jobs.utils.Log;

/**
 * Created by Vinod Madushanka
 */

public class MainAcivity extends PlixBaseActivity implements CustomOkHTTPRequest.RequestListener{

    private String TAG = "MainAcivity";
    private CountDownTimer splashTimer = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_splash);

        getSupportActionBar().hide();

        SuperActivityToast.onRestoreState(this, savedInstanceState,
                ListenerUtils.newInstance()
                        .putListener("splash_acticvity", onButtonClickListener));

        splashTimer = new CountDownTimer(1000,3000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
               goToJobsActivity();
            }
        };

        // Set Data Retrieve type inside  GeneralSettings.class
        if(GeneralSettings.IS_REMOTE_SERVER) {
            attemptRetrieveDataRemote();
        }else{
            attemptRetrieveDataLocal();
        }
    }

    private void goToJobsActivity(){
        Intent intent = new Intent(this, JobsActivity.class);
        startActivity(intent);
        finish();
    }
    private void restartActivity(){
        Intent intent = new Intent(this, MainAcivity.class);
        startActivity(intent);
        finish();
    }

    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        if(GeneralSettings.IS_REMOTE_SERVER) {
            RequestGenerator.generateRetrieveJobsJson(this, this, PlixOkHttpClient.REQUEST_TAG_RETRIEVE_JOBS_JSON).cancelRequest();
        }
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onResponse(String response, long requestTag) {
        Log.show(TAG,"DATA Came From remote host",Log.INFO_LOG);
        if(!response.equals(null)){
            new CustomCache(this).setLocalCache("job_json_data",response);
            splashTimer.start();
        }else{
            errorToast();
        }
    }

    @Override
    public void onError(String message, long requestTag) {
        Log.show(TAG,"OKhttp Error >> "+message,Log.ERROR_LOG);
        errorToast();
    }

    // Get Json Data From a Remote Link
    private void attemptRetrieveDataRemote() {
        RequestGenerator.generateRetrieveJobsJson(this,  this, PlixOkHttpClient.REQUEST_TAG_RETRIEVE_JOBS_JSON).sendRequest(this);
    }

    // Get Json Data From a Local File
    private void attemptRetrieveDataLocal() {
        String raw_data = RawManager.readFile(this, R.raw.jobsearch);
        if(!raw_data.equals(null)){
            new CustomCache(this).setLocalCache("job_json_data",raw_data);
            splashTimer.start();
        }else{
            errorToast();
        }
        Log.show(TAG,"DATA Came From local file",Log.INFO_LOG);
    }

    @SuppressLint("ResourceAsColor")
    private void errorToast(){
        SuperActivityToast.create(this, new Style(), Style.TYPE_BUTTON)
                .setButtonText("Retry")
                .setOnButtonClickListener("splash_acticvity", null, onButtonClickListener)
                .setProgressBarColor(Color.WHITE)
                .setText("Error loading data.")
                .setDuration(Style.DURATION_VERY_LONG)
                .setFrame(Style.FRAME_LOLLIPOP)
                .setColor(R.color.mainColor)
                .setAnimations(Style.ANIMATIONS_FLY).show();
    }
    private final SuperActivityToast.OnButtonClickListener onButtonClickListener =
            new SuperActivityToast.OnButtonClickListener() {

                @Override
                public void onClick(View view, Parcelable token) {
                    SuperToast.create(view.getContext(), "Retry!", Style.DURATION_SHORT)
                            .setPriorityLevel(Style.PRIORITY_HIGH).show();
                    restartActivity();
                }
            };

}
