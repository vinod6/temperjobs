package net.gapstars.temper.jobs.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;

import net.gapstars.temper.jobs.R;
import net.gapstars.temper.jobs.base.PlixBaseActivity;

/**
 * Created by Vinod Madushanka
 */

public class JobsActivity extends PlixBaseActivity {

    private String TAG = "JobsActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listing_layout);
    }

    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
       super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
