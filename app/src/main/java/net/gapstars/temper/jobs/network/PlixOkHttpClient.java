package net.gapstars.temper.jobs.network;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Created by Vinod Madushanka
 */

public class PlixOkHttpClient {

    public static final int REQUEST_TAG_RETRIEVE_JOBS_JSON = 4344;

    // -- Custom Okhttp Client with cache settings
    public static OkHttpClient chachedClient(Context context){

        Cache cache = new Cache(new File(context.getCacheDir(), "http-cache"), 10 * 1024 * 1024 * 5);
        //CustomDns customDns = new CustomDns();

        Interceptor interceptor = new Interceptor() {
            @Override public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed( chain.request() );
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge( 10, TimeUnit.MINUTES )
                        .build();

                return response.newBuilder()
                        .header("Cache-Control", cacheControl.toString() )
                        .build();
            }
        };

        OkHttpClient client = new OkHttpClient();
        client = new OkHttpClient
                .Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(45, TimeUnit.SECONDS)
                .readTimeout(45,TimeUnit.SECONDS)
               // .dns(customDns)// DNS Inject here
                .build();

        return client;
    }




}
