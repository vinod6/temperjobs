package net.gapstars.temper.jobs.network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;

import net.gapstars.temper.jobs.R;
import java.util.HashMap;




public class RequestGenerator {

    @SuppressLint("HardwareIds")
    public static CustomOkHTTPRequest generateRetrieveJobsJson(Context context, CustomOkHTTPRequest.RequestListener listener, int requestTag) {

        HashMap<String, String> params = new HashMap<>();
        params.put("serial", Build.SERIAL);

        String url = context.getResources().getString(R.string.API_JOBS_JSON_REMOTE);

        CustomOkHTTPRequest request = new CustomOkHTTPRequest(context, url, params, listener, requestTag);
        return request;
    }
}
